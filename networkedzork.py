from twisted.internet.protocol import Protocol, Factory, ProcessProtocol, connectionDone
from twisted.internet import reactor


class Zork(Protocol):
    def __init__(self):
        self.inputbuffer = ""
        self.receiving = True
        self.shell = None

    def connectionLost(self, reason=connectionDone):
        if self.shell:
            self.shell.transport.loseConnection()

    def connectionMade(self):
        self.transport.write("Get ".encode("utf-8"))
        self.shell = ZorkProcessProtocol(self)
        reactor.spawnProcess(self.shell, '/zork', args=['/zork'], usePTY=True)

    def dataReceived(self, data):
        """
        As soon as any data is received, write it back.
        """
        if self.shell:
            self.shell.transport.write(data)


class ZorkProcessProtocol(ProcessProtocol):
    def __init__(self, server: Zork):
        self.server = server

    def connectionMade(self):
        self.server.transport.write(b"\n")

    def outReceived(self, data):
        self.server.transport.write(data)

    def errReceived(self, data):
        self.server.transport.write(b"\x1B[31m")
        self.server.transport.write(data)
        self.server.transport.write(b"\x1B[0m")

    def inConnectionLost(self):
        self.server.transport.write(b"\x1B[31mTerminating: STDIN Closed\n")
        print("\x1B[31mTerminating: STDIN Closed")
        self.server.transport.loseConnection()
        self.transport.loseConnection()

    def errConnectionLost(self):
        self.server.transport.write(b"\x1B[31mTerminating: STDERR Closed\n")
        print("\x1B[31mTerminating: STDERR Closed")
        self.server.transport.loseConnection()
        self.transport.loseConnection()

    def outConnectionLost(self):
        self.server.transport.write(b"\x1B[31mTerminating: STDOUT Closed\n")
        print("\x1B[31mTerminating: STDOUT Closed")
        self.server.transport.loseConnection()
        self.transport.loseConnection()

    def processEnded(self, reason):
        self.server.transport.write(b"\x1B[31mTerminating: Process Ended\n")
        self.server.transport.loseConnection()
        self.transport.loseConnection()

    def processExited(self, reason):
        self.server.transport.loseConnection()
        self.transport.loseConnection()


def main():
    f = Factory()
    f.protocol = Zork
    reactor.listenTCP(8008, f)
    reactor.run()


if __name__ == '__main__':
    print("Starting Sudoku Shell Server Debug")
    main()
