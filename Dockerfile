FROM python:3.6.6-stretch
RUN apt-get update && apt-get install -y ca-certificates bind9 build-essential libncurses5-dev
RUN pip install --upgrade pip

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY ./*.c ./
COPY ./*.h ./
COPY ./*.dat ./
COPY Makefile ./
RUN make

COPY networkedzork.py ./

CMD python networkedzork.py
